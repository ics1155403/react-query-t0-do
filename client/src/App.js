import "./components/app.css"
import { List } from './components/List';
import { Todo } from './components/Todo';

function App() {
  return (
    <>
      <div className="main">
        <Todo />
        <List />
      </div>
    </>
  );
}

export default App;
