import React, { useState } from 'react'
import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query"

export const List = () => {
    const [data, setData] = useState()
    const queryClient = useQueryClient()

    const fetchTodo = async () => {
        (await fetch("http://localhost:8000/todo")).json()
            .then(data => setData(data.data))
    }

    const deleteTodo = async (index) => {
        // console.log(index)
        await fetch("http://localhost:8000/todo/delete", {
            method: "DELETE",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ index })
        })
    }

    const query = useQuery({
        queryKey: ["todo"],
        queryFn: fetchTodo
    })

    const mutation = useMutation({
        mutationFn: deleteTodo,
        onSuccess: () => {
            console.log("data deleted")
            queryClient.invalidateQueries({ queryKey: ['todo'] })
        }
    })


    return (
        <>
            <div className="listContainer">
                {data && data.map((elem, index) => {
                    return <div key={index}>
                        <li >{elem.title}</li>
                        <button onClick={() => mutation.mutate(index)}>Delete</button>
                    </div>
                })}
            </div>
        </>
    )
}
