import React, { useState } from 'react'
import { useMutation, QueryClient } from "@tanstack/react-query"

const addTodo = async (text) => {
    await fetch("http://localhost:8000/todo/create", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ title: text })
    })
}

export const Todo = () => {
    const [text, setText] = useState("");

    const mutation = useMutation({
        mutationFn: addTodo,
        onSuccess: () => {
            console.log("todo added")
        },
        onError: () => {
            console.log("error on todo adding")
        }
    })

    return (
        <>
            <div className="container">
                <form onSubmit={() => mutation.mutate(text)}>
                    <input type='text' placeholder="What needs to be done?" onChange={e => setText(e.target.value)} />
                    <button type='submit'>Add ToDo</button>
                </form>
            </div>
        </>
    )
}
